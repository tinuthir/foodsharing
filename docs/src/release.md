# Releasing

We want to release the beta version of the foodsharing website to production every two months.
For one to two weeks, we stop merging features and focus on testing and bug fixing. If there are features not to be used for the next release, they are disabled.
If there are still release blockers, we will talk about them in a call and decide what to do.

As soon as everything is ready, the master branch can be merged into the production branch.
People with rights for this can be found in the [gitlab-Wiki](https://gitlab.com/foodsharing-dev/foodsharing/-/wikis/responsibilities).

For organization, we use [gitlab milestones](https://gitlab.com/foodsharing-dev/foodsharing/-/milestones). Their titles consist of the month and the year of the release and a fruit. If you are working on something which belongs to the next release, feel free to add it to the milestone. 
Please don't recycle milestones, but always use a new one for each release.