<?php

namespace Foodsharing\Modules\Core\DBConstants\Content;

/*
 *
 * At the beginning of this class there were up to 49 content ids given as number
 * please replace them by constants to make them easier to read.
 *
 * You can find them by searching for the use of `$this->contentGateway->get()`
 * */

class ContentId
{
	public const PARTNER_PAGE_10 = 10;
	public const QUIZ_DESCRIPTION_PAGE_12 = 12;
	public const QUIZ_FAILED_PAGE_13 = 13;
	public const QUIZ_CONFIRM_FS_PAGE_14 = 14;
	public const QUIZ_CONFIRM_SM_PAGE_15 = 15;
	public const QUIZ_CONFIRM_AMB_PAGE_16 = 16;
	public const QUIZ_START_PAGE_17 = 17;
	public const QUIZ_POPUP_PAGE_18 = 18;
	public const QUIZ_FAILED_FS_TRY_1_PAGE_19 = 19;
	public const QUIZ_FAILED_FS_TRY_2_PAGE_20 = 20;
	public const QUIZ_FAILED_FS_TRY_3_PAGE_21 = 21;
	public const QUIZ_FAILED_SM_TRY_1_PAGE_22 = 22;
	public const QUIZ_FAILED_SM_TRY_2_PAGE_23 = 23;
	public const QUIZ_FAILED_SM_TRY_3_PAGE_24 = 24;
	public const QUIZ_FAILED_AMB_TRY_1_PAGE_25 = 25;
	public const QUIZ_FAILED_AMB_TRY_2_PAGE_26 = 26;
	public const QUIZ_FAILED_AMB_TRY_3_PAGE_27 = 27;
	public const QUIZ_REMARK_PAGE_33 = 33;
	public const QUIZ_POPUP_SM_PAGE_34 = 34;
	public const QUIZ_POPUP_AMB_PAGE_35 = 35;
	public const QUIZ_POPUP_AMB_LAST_PAGE_36 = 36;

	public const SUPPORT_FOODSHARING_PAGE_42 = 42;
	public const PRIVACY_POLICY_CONTENT = 28;
	public const START_MAIN_AU_PAGE_37 = 37;
	public const START_MAIN_DE_PAGE_38 = 38;
	public const TEAM_HEADER_PAGE_39 = 39;
	public const START_BETA_PAGE_48 = 48;
	public const TEAM_ACTIVE_PAGE_53 = 53;
	public const TEAM_FORMER_ACTIVE_PAGE_54 = 54;
	public const PRIVACY_NOTICE_CONTENT = 64;
	public const BROADCAST_MESSAGE = 51;
}
